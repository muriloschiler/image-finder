import os
import unittest

from flask import current_app
from flask_testing import TestCase

from debian_image_finder import app


class TestConfig(TestCase):
    def create_app(self):
        app.config.from_object('config.DevelopmentConfig')
        return app

    def test_app(self):
        self.assertTrue(app.config['SECRET_KEY'] == 'my_precious')
        self.assertFalse(current_app is None)
        self.assertTrue(app.config['DEBUG'])
        self.assertTrue(app.config['DEVELOPMENT'])
        self.assertFalse(app.config['SQLALCHEMY_TRACK_MODIFICATIONS'])
        sqlalchemy_db_uri = app.config['SQLALCHEMY_DATABASE_URI']
        database_url = os.environ.get('DATABASE_URL')
        self.assertTrue(sqlalchemy_db_uri == database_url)


if __name__ == '__main__':
    unittest.main()
