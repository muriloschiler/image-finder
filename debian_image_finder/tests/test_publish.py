import json

from debian_image_finder.tests.base import BeforeTestUserCommon
from debian_image_finder.models import Provider
from debian_image_finder import db


class TestPublishService(BeforeTestUserCommon):
    """Tests for the Publish Service."""

    def test_publish(self):

        provider = Provider(name='provider',
                            description='test',
                            markdown_file='file')
        db.session.add(provider)
        db.session.commit()

        args = {
            "items": [
                {
                    "data": {
                        "provider": "cloud.debian.org",
                        "ref": "ref.tar.xz"
                    },
                    "kind": "Upload",
                    "metadata": {
                        "labels": {
                            "cloud.debian.org/vendor": "vendor",
                            "cloud.debian.org/version": "version",
                            "debian.org/arch": "arch",
                            "debian.org/release": "release",
                            "upload.cloud.debian.org/image-format": "internal",
                            "upload.cloud.debian.org/type": "release"
                        },
                        "uid": "uid"
                    }
                }
            ]
        }

        headers = {
            'Content-Type': 'application/json',
            'x-access-token': self.token
        }

        response = self.client.post('/api/publish/' + str(provider.id),
                                    data=json.dumps(args),
                                    headers=headers)

        data = json.loads(response.data.decode())
        image = data['images'][0]
        self.assertEqual(response.status_code, 200)
        self.assertIn('arch', image['arch'])
        self.assertIn('ref.tar.xz', image['artifact_url'])
        self.assertIn('release', image['img_type'])
        self.assertIn('ref', image['ref'])
        self.assertIn('all', image['region'])
        self.assertIn('arch', image['arch'])
        self.assertIn('release', image['release'])
        self.assertIn('vendor', image['vendor'])
        self.assertIn('version', image['version'])
        self.assertIn('', image['packages'])
        self.assertEqual(1, image['provider_id'])
