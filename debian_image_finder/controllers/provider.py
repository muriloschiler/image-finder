from debian_image_finder import app
from debian_image_finder import db
from flask import request, jsonify
from debian_image_finder.models import Provider
from debian_image_finder.models import provider_schema, providers_schema
from debian_image_finder.controllers.user import token_required


# endpoint to create new provider
@app.route("/api/provider", methods=["POST"])
@token_required
def add_provider(user):
    name = request.json['name']
    description = request.json['description']
    markdown_file = request.json['markdown_file']
    new_provider = Provider(name, description, markdown_file)

    db.session.add(new_provider)
    db.session.commit()

    return provider_schema.jsonify(new_provider)


# endpoint to show all providers
@app.route("/api/provider", methods=["GET"])
def get_provider():
    all_providers = Provider.query.all()
    result = providers_schema.dump(all_providers)
    return jsonify(result)


# endpoint to get provider detail by id
@app.route("/api/provider/<id>", methods=["GET"])
def provider_detail(id):
    provider = Provider.query.get(id)
    return provider_schema.jsonify(provider)


# endpoint to update provider
@app.route("/api/provider/<id>", methods=["PUT"])
@token_required
def provider_update(user, id):
    provider = Provider.query.get(id)
    name = request.json['name']
    description = request.json['description']
    markdown_file = request.json['markdown_file']

    provider.name = name
    provider.description = description
    provider.markdown_file = markdown_file

    db.session.commit()
    return provider_schema.jsonify(provider)


# endpoint to delete provider
@app.route("/api/provider/<id>", methods=["DELETE"])
@token_required
def provider_delete(user, id):
    provider = Provider.query.get(id)
    db.session.delete(provider)
    db.session.commit()

    return provider_schema.jsonify(provider)
