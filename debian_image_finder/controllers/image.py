from debian_image_finder import app
from debian_image_finder import db
from flask import request, jsonify
from debian_image_finder.models import Image, image_schema, images_schema
from debian_image_finder.controllers.user import token_required
from datetime import datetime


# endpoint to create new image
@app.route("/api/image", methods=["POST"])
@token_required
def add_image(user):
    arch = request.json['arch']
    release = request.json['release']
    img_type = request.json['img_type']
    vendor = request.json['vendor']
    version = request.json['version']
    region = request.json['region']
    ref = request.json['ref']
    packages = request.json['packages']
    created_at = datetime.utcnow()
    provider_id = request.json['provider_id']

    new_image = Image(arch,
                      release,
                      img_type,
                      vendor,
                      version,
                      region,
                      ref,
                      packages,
                      created_at,
                      provider_id)

    db.session.add(new_image)
    db.session.commit()

    return image_schema.jsonify(new_image)


# endpoint to show all images
@app.route("/api/image", methods=["GET"])
def get_image():
    all_images = Image.query.all()
    result = images_schema.dump(all_images)
    return jsonify(result)


# endpoint to get image detail by id
@app.route("/api/image/<id>", methods=["GET"])
def image_detail(id):
    image = Image.query.get(id)
    return image_schema.jsonify(image)


# endpoint to update image
@app.route("/api/image/<id>", methods=["PUT"])
@token_required
def image_update(user, id):
    image = Image.query.get(id)
    arch = request.json['arch']
    release = request.json['release']
    img_type = request.json['img_type']
    vendor = request.json['vendor']
    version = request.json['version']
    region = request.json['region']
    ref = request.json['ref']
    packages = request.json['packages']

    image.arch = arch
    image.release = release
    image.img_type = img_type
    image.vendor = vendor
    image.version = version
    image.region = region
    image.ref = ref
    image.packages = packages

    db.session.commit()
    return image_schema.jsonify(image)


# endpoint to delete image
@app.route("/api/image/<id>", methods=["DELETE"])
@token_required
def image_delete(user, id):
    image = Image.query.get(id)
    db.session.delete(image)
    db.session.commit()

    return image_schema.jsonify(image)
