import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager, Server
from flask_migrate import Migrate, MigrateCommand
from flask_marshmallow import Marshmallow
from flask_seeder import FlaskSeeder

app = Flask(__name__)

# Load the default configuration
app.config.from_object(os.environ.get("APP_SETTINGS"))

db = SQLAlchemy(app)
ma = Marshmallow(app)
migrate = Migrate(app, db)

server = Server(host="0.0.0.0", port=5000)

manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('runserver', server)

seeder = FlaskSeeder()
seeder.init_app(app, db)

from debian_image_finder.controllers import default
from debian_image_finder.controllers import user
from debian_image_finder.controllers import provider
from debian_image_finder.controllers import image
from debian_image_finder.controllers import publish
from debian_image_finder.models import Provider, Image
