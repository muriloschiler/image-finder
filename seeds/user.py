from flask_seeder import Seeder
from debian_image_finder.models import User
from debian_image_finder import db
import uuid
from werkzeug.security import generate_password_hash
import json


class UserSeeder(Seeder):

    def run(self):
        json_file = open('seed.json')
        data = json.load(json_file)

        for user in data['users']:
            print('Name: ' + user['name'])
            print('Password: ' + user['password'])
            print('Admin: ' + str(user['admin']))
            print('')
            hashed_password = generate_password_hash(user['password'],
                                                     method='sha256')
            db.session.add(User(public_id=str(uuid.uuid4()),
                                name=user['name'],
                                password=hashed_password,
                                admin=user['admin']))
